# AdmBootsApi

#### Description
AdmBoots 基于.NET Core生态系统最成熟技术体系，借鉴众多开源项目的优点，旨在打造前后端分离快速开发框架，让你开发Web管理系统和移动端Api更简单。
框架对依赖注入、日志、缓存、模型映射、认证/授权、仓储、健康检测、ORM，任务调度等模块进行更高一级的自动化封装。
📌项目特点：代码简洁、易上手、学习成本低、开箱即用
📌适用人群：.NetCore入门同学，正在寻找具有以上特点框架的同学

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
